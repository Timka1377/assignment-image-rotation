#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/img.h"
#include "../include/structs.h"
#pragma once

// Алгоритм, по которому совершается поворот
void alg_rot(struct imgdata pix, struct imgdata rotation);

// Функуция поворота
struct imgdata img_turn(struct imgdata pix);
