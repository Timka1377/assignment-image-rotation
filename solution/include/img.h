#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/structs.h"
#pragma once

// Создание структуры imgdata
struct imgdata create_img_data(uint32_t width, uint32_t height);

// Получение padding
long padding(uint64_t width);

// Очищаем выделенную память
void clean_memory(struct imgdata pix);
