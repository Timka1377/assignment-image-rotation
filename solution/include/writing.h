#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/check.h"
#include "../include/img.h"
#include "../include/structs.h"
#pragma once

// Функции для записи данных в bmp файл
    // Отметка формата
void format(FILE * file);
    // Размер файла
void file_size(struct imgdata pix, FILE * file);
    // Зарезервированно
void reserved(FILE * file);
    // Положение пиксельных данных относительно начала файла
void position(FILE * file);
    // Размер структуры
void structure_size(FILE * file);
    // Ширина изображения
void image_width(struct imgdata pix, FILE * file);
    // Высота изображения
void image_height(struct imgdata pix, FILE * file);
    // Значение формата
void format_value(FILE * file);
    // Бит на пиксель
void bits_per_pixel(FILE * file);
    // Метод компрессии
void compression(FILE * file);
    // Размер пиксельных данных в байтах
void size_in_bytes(FILE * file);
    // PPM по горизонтали и вертикали
void PPM(FILE * file);
    // Размер таблицы цветов в ячейках
void color_chart_size(FILE * file);
    // Количество ячеек от начала таблицы цветов до последней используемой
void number_of_cells(FILE * file);
    // Заполнение пиксельными данными
void filling(struct imgdata pix, FILE * file);

// Запись данных в bmp файл
enum write_status save (struct imgdata pix, FILE * file);
