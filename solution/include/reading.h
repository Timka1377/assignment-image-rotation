#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/check.h"
#include "../include/img.h"
#include "../include/structs.h"
#pragma once

// Функции для считывания данных bmp файла
    // Смещение пиксельных данных
uint32_t offset(FILE * file, uint32_t pixel_data_offset);
    // Читаем пиксельные данные и записываем в структуру
void reading(FILE * file, struct imgdata pix);
    // Количество битов на пиксель
uint16_t bits(FILE * file, uint16_t bits_per_pixel);

// Считывание данных bmp файла
enum read_status load(struct imgdata *pix, FILE * file);
