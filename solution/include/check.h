#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/structs.h"
#pragma once

enum read_status  {
  READ_OK = 0,
  READ_INVALID_BITS,
  READ_INVALID_HEADER
  };

enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
};
// Проверяем на ошибки при сячитывании из файла
enum read_status read_check_errors(uint32_t pixel_data_offset, 
                int32_t width,int32_t height, uint16_t bits_per_pixel);
// Проверяем успешную или неуспешную запись в файл 
enum write_status write_check_errors(FILE * file);
