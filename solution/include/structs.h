#include <stdint.h>
#include <stdio.h>
#pragma once

//Структура rgb-пикселя
struct pixel
{
	uint8_t r, g, b;
};

//Структура пиксельных данных изображения
struct imgdata 
{
	uint32_t width, height;
	struct pixel * data; 
};
