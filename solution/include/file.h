#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/structs.h"
#pragma once

// Открытие файла
FILE * open(const char * filename, const char * arg);

// Закрытие файла
void close(FILE * file);
