#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


#include "../include/check.h"
#include "../include/file.h"
#include "../include/img.h"
#include "../include/reading.h"
#include "../include/structs.h"
#include "../include/turn.h"
#include "../include/writing.h"


int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    if (argc != 3)
    {
        printf("Invalid counts of arguments.");
        return 1;
    }

    struct imgdata pix = {0, 0, NULL};

    const char * filename_read = argv[1];
    FILE * file_read = open(filename_read, "rb");
    enum read_status read_check_errors = load(&pix, file_read);
    if(read_check_errors != READ_OK){
        return read_check_errors;
    }
    close(file_read);

    struct imgdata rotation = img_turn(pix);

    const char * filename_fill = argv[2];
    FILE * file_fill = open(filename_fill, "wb");
    enum write_status write_check_errors = save(rotation, file_fill);
    if(write_check_errors != WRITE_OK){
        return write_check_errors;
    }
    close(file_fill);

    clean_memory(pix);
    clean_memory(rotation);

    return 0;
}
