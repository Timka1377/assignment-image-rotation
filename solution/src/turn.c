#include "../include/turn.h"

void alg_rot(struct imgdata pix, struct imgdata rotation){
    for (size_t i = 0; i < pix.height; i++){
        for (size_t j = 0; j < pix.width; j++){
            rotation.data[(j * rotation.width) + (rotation.width - i - 1)] = pix.data[i * pix.width + j];
        }
    }
}

struct imgdata img_turn(struct imgdata pix){
    struct imgdata rotation = create_img_data(pix.height, pix.width);
    alg_rot(pix, rotation);
    return rotation;
}
