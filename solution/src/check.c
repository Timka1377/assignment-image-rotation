#include "../include/check.h"

enum read_status read_check_errors(uint32_t pixel_data_offset, 
                int32_t width,int32_t height, uint16_t bits_per_pixel){

    if ( pixel_data_offset == 0 && width == 0 && height == 0 && 
        bits_per_pixel == 0){
            return READ_INVALID_HEADER;
        }

    else if (bits_per_pixel != 24){
        return READ_INVALID_BITS;
    }

    else {
        return READ_OK;
    }
}

enum write_status write_check_errors(FILE * file){
    if(!feof(file)){
        return WRITE_OK;
    }
    else{
        return WRITE_ERROR;
    }
}
