#include "../include/file.h"

FILE * open(const char * filename, const char * arg) {
    FILE * file;
    file = fopen(filename, arg);
    return file;
}

void close(FILE * file){
    fclose(file);
}
