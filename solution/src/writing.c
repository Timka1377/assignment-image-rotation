#include "../include/writing.h"

void format(FILE * file){
	fputc ('B', file);
	fputc ('M', file);
}

void file_size(struct imgdata pix, FILE * file){
	uint32_t tmp = 14 + 40 + pix.width * pix.height * 24;
	fwrite (&tmp, sizeof(uint32_t), 1, file);
}

void reserved(FILE * file){
	for (size_t i = 0; i < 4; i++){
        fputc (0, file);
    }
}
 
void position(FILE * file){
	uint32_t tmp = 14 + 40;
	fwrite (&tmp, sizeof(uint32_t), 1, file);
}

void structure_size(FILE * file){
	uint32_t tmp = 40;
	fwrite (&tmp, sizeof(uint32_t), 1, file);
}
 
void image_width(struct imgdata pix, FILE * file){
	int32_t tmp = (int32_t) pix.width;
	fwrite (&tmp, sizeof(int32_t), 1, file);
}
 
void image_height(struct imgdata pix, FILE * file){
	int32_t tmp = (int32_t) pix.height;
	fwrite (&tmp, sizeof(int32_t), 1, file);
}

void format_value(FILE * file){
	uint16_t tmp = 1;
	fwrite (&tmp, sizeof(uint16_t), 1, file);
}
 
void bits_per_pixel(FILE * file){
	uint16_t tmp = 24;
	fwrite (&tmp, sizeof(uint16_t), 1, file);
}
 
void compression(FILE * file){
	uint32_t tmp = 0;
	fwrite (&tmp, sizeof(uint32_t), 1, file);
}
 
void size_in_bytes(FILE * file){
	uint32_t tmp = 0; 
	fwrite (&tmp, sizeof(uint32_t), 1, file);
}

void PPM(FILE * file){
	int32_t tmp = 3780;
	fwrite (&tmp, sizeof(int32_t), 1, file);
	fwrite (&tmp, sizeof(int32_t), 1, file);
}
 
void color_chart_size(FILE * file){
	uint32_t tmp = 0;
	fwrite (&tmp, sizeof(uint32_t), 1, file);
}

void number_of_cells(FILE * file){
	uint32_t tmp = 0;
	fwrite (&tmp, sizeof(uint32_t), 1, file);
}

void filling(struct imgdata pix, FILE * file){
	struct pixel null_bytes[] = {{0},{0},{0}};
	for (size_t i = 0; i < pix.height; i++) {
        fwrite(&(pix.data[i * pix.width]), sizeof(struct pixel), pix.width, file); 
        fwrite(null_bytes, 1, padding(pix.width), file);
    }
}


enum write_status save (struct imgdata pix, FILE * file) {

	format(file);
	file_size(pix, file);
	reserved(file);
	position(file);
    structure_size(file);
	image_width(pix, file);
	image_height(pix, file);
    format_value(file);
	bits_per_pixel(file);
	compression(file);
	size_in_bytes(file);
    PPM(file);
	color_chart_size(file);
    number_of_cells(file);
    filling(pix, file);

	enum write_status result = write_check_errors(file); 
	return result;
}
