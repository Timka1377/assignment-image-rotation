#include "../include/reading.h"

uint32_t offset(FILE * file, uint32_t pixel_data_offset){
    fseek (file, 10, SEEK_SET);
	fread (&pixel_data_offset, sizeof(uint32_t), 1, file);
    return pixel_data_offset;
}

void reading(FILE * file, struct imgdata pix){
	for (uint32_t i = 0; i < pix.height; i++) {
            fread(&(pix.data[i * pix.width]), sizeof(struct pixel), pix.width, file);
            fseek(file, padding(pix.width), SEEK_CUR);
    }
}

uint16_t bits(FILE * file, uint16_t bits_per_pixel){
    fseek (file, 28, SEEK_SET);
    fread (&bits_per_pixel, sizeof(uint16_t), 1, file);
    return bits_per_pixel;
}

enum read_status load(struct imgdata *pix, FILE * file){

    uint32_t pixel_data_offset = 0;
	int32_t width = 0;
    int32_t height = 0;
    uint16_t bits_per_pixel = 0;

    pixel_data_offset = offset(file, pixel_data_offset);

    fseek (file, 18, SEEK_SET);	
	fread (&width, sizeof(int32_t), 1, file);
	fread (&height, sizeof(int32_t), 1, file);

    bits_per_pixel = bits(file, bits_per_pixel);

	*pix = create_img_data(width, height);

    fseek (file, pixel_data_offset, SEEK_SET);
    
	reading(file, *pix);

    enum read_status result = read_check_errors(pixel_data_offset, width, height, bits_per_pixel);

    return result;
}
