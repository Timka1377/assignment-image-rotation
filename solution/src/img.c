#include "../include/img.h"

struct imgdata create_img_data(uint32_t width, uint32_t height)
{
    struct imgdata pix = {width, height, NULL};
    pix.data = malloc(width * height * sizeof(struct pixel));
    return pix;
}

long padding(uint64_t width)
{
    long padding = ((long)(4 - (width * sizeof(struct pixel)) % 4) % 4);
    return padding;
}

void clean_memory(struct imgdata pix){
	free(pix.data);
}
